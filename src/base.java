
abstract public class base {

	public int id;
	private static int nid=1;
	
	base(){
		id = nid++;
	}
	
	base(int id){
		this.id = id;
		if(nid <= id) {
			nid = id+1;
		}
	}
	
	abstract public String getIdType();
	
}
