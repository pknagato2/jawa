import java.time.LocalDate;
import java.util.ArrayList;

public class Task extends base implements Comparable<Task>{
    private String name;
    private LocalDate time;
    private ArrayList <Person> persons = new ArrayList<>();
    
    public Task(String name) {
        super();
    	this.name = name;
        time = LocalDate.now();

    }
    
    
    public LocalDate getTime() {
    	return time;
    }
    
    
    @Override
    public int compareTo(Task task) {
        return task.time.compareTo(this.time);
    }
    
    public boolean equals(Task task) {
    	if(task.id == this.id)
    		return true;
    	return false;
    }
    
    public Task(String name, String dateString) {
        super();
    	this.name = name;
        time = LocalDate.parse(dateString);
        }

    @Override
    public String getIdType() {
    	return "TaskId";
    }
    
    
    public Task(String name, int id) {
        super(id);
    	this.name = name;
        this.id = id;
        time = LocalDate.now();
    }
    
    public Task(String name, int id, String dateString) {
        super(id);
    	this.name = name;
        this.id = id;
        time = LocalDate.parse(dateString);
    }
    
    
    
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    
    public void ShowDetails() {
        System.out.print("Name; " + name );
    }
    
    public int getId() {
    	return id;
    }
    
    public void add(Person person) {
    	persons.add(person);
    }
    
    public String toString() {
    	String str = "";
    	str = str + "Task: " + id + " " + name + " " + time.toString() + "\n";
    	for (Person person : persons) {
    		str = str + "		" + person.ToString() + "\n";
    	}
    	return str;
    }
    
    public static Task getTestFromString(String stringData) {
    	String [] arrOfStr = stringData.split(" ");
    	return new Task(arrOfStr[2], Integer.parseInt(arrOfStr[1]), arrOfStr[3]);
    }
}
