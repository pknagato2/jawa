import java.util.ArrayList;


class Project extends base {
    private String name;
    private ArrayList< Person> persons = new ArrayList<>();
    private ArrayList< Task> tasks = new ArrayList<>();
    
    Project(String s) {
    	super();
        name = s;
        }
    
    Project(String s, int id) {
        super(id);
    	name = s;
    }
    
    @Override
    public String getIdType() {
    	return "ProjectId";
    }
    
    public boolean equals(Project project) {
    	if(project.id == this.id)
    		return true;
    	return false;
    }
    
    public int getId() {
    	return id;
    }

    public void AddPerson(Person person) {
        persons.add(person);
    }
    public void RemovePerson(Person person) {
        persons.removeIf(proj_person -> proj_person.getName().equals(person.getName()) &&
                         proj_person.getLastName().equals(person.getLastName()));
    }
    
    public void AddTask(Task task) {
        tasks.add(task);
    }
    public void RemoveTask(Task task) {
        tasks.removeIf(proj_task -> proj_task.getName().equals(task.getName()));
    }

    String getName() {
        return name;
    }

    public ArrayList<Task> getTasks() {
        return tasks;
    }

    public void ShowDetails() {
        System.out.println("Project name: " + name);
        System.out.println("People involved in project: ");
        for (Person person : persons)
            person.ShowDetails();
        System.out.println("Tasks: ");
        for (Task task : tasks)
            task.ShowDetails();
    }

    public void removePerson(String name, String lastName) {
        persons.removeIf(person -> person.getName().equals(name) &&
                         person.getLastName().equals(lastName));
    }

    public void removeTask(String taskName) {
        tasks.removeIf(task -> task.getName().equals(taskName));
    }

    public ArrayList<Person> getPersons() {
        return persons;
    }

    public void setName(String s) {
        name = s;
    }

    public Task getTask(String name) {
        return tasks.stream()
                .filter(task -> task.getName().equals(name))
                .findFirst().get();
    }
    
    public String ToString() {
    	String str = "";
    	str = str + "Project: " + id + " " + name + "\n"; 
    	
    	for(Task task : tasks) {
    		str = str + "	" + task.toString() + "\n";
    	}
    			
    	return str;
    }
    
    public static Project getProjectFromString(String stringData) {
    	String [] arrOfStr = stringData.split(" ");
    	return new Project(arrOfStr[2], Integer.parseInt(arrOfStr[1]));
    }
    
    
    
    
}