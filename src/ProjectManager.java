import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Reader;
import java.lang.reflect.Array;
import java.nio.file.FileSystemException;
import java.time.LocalDate;
import java.util.*;

import sun.util.resources.LocaleData;

class ProjectManager
{
    private ArrayList<Project> projects = new ArrayList<>();
    private ArrayList<Person> persons = new ArrayList<>();

    void addProject(Project project) {
        projects.add(project);
    }

    public void printAsc() { 
    	ArrayList <Task> tasks= getTasks();
    	Collections.sort(tasks);
    	System.out.print("Descending Tasks: \n");
    	for(Task task : tasks) {
    		System.out.print(task.getId() + " " + task.getName() + " " + task.getTime().toString() + "\n");
    	}
    }
    
    public void printDesc() { 
    	ArrayList <Task> tasks= getTasks();
    	Collections.sort(tasks);
    	Collections.reverse(tasks);
    	System.out.print("Ascending Tasks: \n");
    	for(Task task : tasks) {
    		System.out.print(task.getId() + " " + task.getName() + " " + task.getTime().toString() + "\n");
    	}
    }
    
    public void printTardy() {
    	ArrayList <Task> tasks = getTasks();
    	System.out.print("Late Tasks: \n");
    	for (Task task : tasks) {
    		if(task.getTime().compareTo(LocalDate.now()) <= 0) {
    			System.out.print(task.getId() + " " + task.getName() + " " + task.getTime().toString() + "\n");
    		}
    	}
    }
    
    void addPerson(Person person) {
        persons.add(person);
    }
    
    public ArrayList<Project> getProjects() {
        return projects;
    }
    
    public ArrayList<Person> getPersons() { return persons; }
    
    public Project getProject(String name) {
        return projects.stream()
                .filter(project -> project.getName().equals(name))
                .findFirst()
                .get();
    }
    
    public Person getPerson(String name , String lastName) {
        return persons.stream()
                .filter(person -> person.getName().equals(name) &&
                        person.getLastName().equals(lastName))
                .findFirst().get();
    }
    
    public void enroll(Person person, Project proj) {
        proj.AddPerson(person);
    }
    
    public void AddTask(Task task, String projName) {
        Project result = getProject(projName);
        result.AddTask(task);
    }

    public void removePerson(Person person) {
        persons.removeIf(person1 -> person1.getName().equals(person.getName()) &&
                            person1.getLastName().equals(person.getLastName()));
        for (Project project : projects) {
            project.removePerson(person.getName(), person.getLastName());
        }
    }
    public void removeProject(String s) {
        projects.removeIf(project -> project.getName().equals(s));
    }

    public void removeTask(String taskName, String projName) {
        Project proj = getProject(projName);
        proj.removeTask(taskName);
    }

    public void showTasks() {
        for (Project project : projects) {
            for (Task task : project.getTasks())
                task.ShowDetails();
        }
    }

    public ArrayList<Task> getTasks() {
        ArrayList<Task> tasks = new ArrayList<>();
        for (Project project : projects) {
            tasks.addAll(project.getTasks());
        }
        return tasks;
    }

    public void fire(Person person, Project project) {
        project.removePerson(person.getName(), person.getLastName());
    }

    public void fire(Person person) {
        persons.removeIf(person1 -> person1.getName().equals(person.getName()) &&
                            person1.getLastName().equals(person.getLastName()));
    }
    
    public void assign(Person person, Task task) {
    	task.add(person);
    }

    public Task getTask(String name, Project proj) {
        return proj.getTask(name);
    }
    
    public void printInfo(String filename) {
    	try (PrintWriter out = new PrintWriter(filename)) {
    		for(Person person : persons) {
    			out.print(person.ToString() + "\n");
    		}
    		
    		out.print("<\n");
    		
    		for(Project project : projects) {	
        		out.print(project.ToString());
        	}
    		
    		out.print(">\n");
    		
    		
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    }
    
    void clear() {
    	persons.clear();
    	projects.clear();
    }
    
    
    public void readFromFile(String filename) {
    	projects.clear();
    	try(FileReader in = new FileReader(filename)){
    		String line = "";
    		BufferedReader bufferedReader = new BufferedReader(in);
    		Project lastProject = null;
    		Task lastTask = null;
    		while(!line.contains("<")) {
    			line = bufferedReader.readLine().toString();
    			if(!line.contains("<")) {
    				persons.add(Person.getPersonFromString(line));
    				System.out.println(line);
    			}else {
    				break;
    			}
    		}
    		
    		while(!line.contains(">")) {
    			line = bufferedReader.readLine().toString();
    			if(!line.contains(">")) {
    			
    				if(line.contains("Project")) {
    					lastProject = Project.getProjectFromString(line);
    					projects.add(lastProject);
    					System.out.println(line);
    				}else if(line.contains("Task")) {
    					lastTask = Task.getTestFromString(line);
    					lastProject.AddTask(lastTask);
    					System.out.println(line);
    				}else if(line.contains("Person")) {
    					lastTask.add(Person.getPersonFromString(line));
    					System.out.println(line);
    				}
    			}
    		}
    		bufferedReader.close();
    	}catch(Exception e) {
    		// NOOP
    	}
    	
    }
    
    
}