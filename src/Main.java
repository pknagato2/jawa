import java.util.NoSuchElementException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ProjectManager pm = new ProjectManager();
        Scanner ins = new Scanner(System.in);
        while (true) {
            System.out.print("jira-admin# ");
            String[] cmd = ins.nextLine().split(" ");
            try {
                if (cmd[0].toLowerCase().equals("quit")) {
                    break;
                } else if (cmd[0].toLowerCase().equals("add")) {
                    if (cmd[1].toLowerCase().equals("person")) {
                        Person person = new Person(cmd[2], cmd[3]);
                        pm.addPerson(person);
                    } else if (cmd[1].toLowerCase().equals("project")) {
                        Project proj = new Project(cmd[2]);
                        pm.addProject(proj);
                    } else if (cmd[1].toLowerCase().equals("task")) {
                    	if (cmd.length < 4) {
                        Task task = new Task(cmd[2]);
                        pm.AddTask(task, cmd[3]);
                        }else {
                    	Task task = new Task(cmd[2], cmd[4]);
                    	pm.AddTask(task, cmd[3]);    	
                    	}
                    }
                } else if (cmd[0].toLowerCase().equals("show")) {
                    if (cmd[1].toLowerCase().equals("project")) {
                        pm.getProject(cmd[2]).ShowDetails();
                    } else if (cmd[1].toLowerCase().equals("person")) {
                        if (cmd.length > 2) {
                            for (Person person : pm.getProject(cmd[2]).getPersons())
                                person.ShowDetails();
                        } else {
                            for (Person person : pm.getPersons())
                                person.ShowDetails();
                        }
                    } else if (cmd[1].toLowerCase().equals("task")) {
                        if (cmd.length > 2) {
                            for (Task task : pm.getProject(cmd[2]).getTasks())
                                task.ShowDetails();
                        } else {
                            for (Task task : pm.getTasks())
                                task.ShowDetails();
                        }
                    }
                } else if (cmd[0].toLowerCase().equals("remove")) {
                    if (cmd[1].toLowerCase().equals("person")) {
                        pm.removePerson(pm.getPerson(cmd[2], cmd[3]));
                    } else if (cmd[1].toLowerCase().equals("project")) {
                        pm.removeProject(cmd[2]);
                    } else if (cmd[1].toLowerCase().equals("task")) {
                        pm.removeTask(cmd[2], cmd[3]);
                    }
                } else if (cmd[0].toLowerCase().equals("enroll")) {
                    pm.enroll(pm.getPerson(cmd[1], cmd[2]), pm.getProject(cmd[3]));
                } else if (cmd[0].toLowerCase().equals("fire")) {
                    pm.fire(pm.getPerson(cmd[1], cmd[2]), pm.getProject(cmd[3]));
                } else if (cmd[0].toLowerCase().equals("modify")) {
                    if (cmd[1].toLowerCase().equals("person")) {
                        Person person = pm.getPerson(cmd[2], cmd[3]);
                        person.setName(cmd[4]);
                        person.setLastName(cmd[5]);
                    } else if (cmd[1].toLowerCase().equals("project")) {
                        Project project = pm.getProject(cmd[2]);
                        project.setName(cmd[3]);
                    } else if (cmd[1].toLowerCase().equals("task")) {
                        Task task = pm.getTask(cmd[2], pm.getProject(cmd[3]));
                        task.setName(cmd[4]);
                    }
                }else if (cmd[0].toLowerCase().equals("export")){
                	pm.printInfo(cmd[1]);
                }else if (cmd[0].toLowerCase().equals("import")){
                	pm.readFromFile(cmd[1]);
                }else if (cmd[0].toLowerCase().equals("assign")) {
                	pm.assign(pm.getPerson(cmd[1], cmd[2]), pm.getTask(cmd[3], pm.getProject(cmd[4])));
                }
                else if (cmd[0].toLowerCase().equals("list")) {
                	if(cmd[2].toLowerCase().equals("asc")) {
                		pm.printAsc();
                	}else if(cmd[2].toLowerCase().equals("desc")) {
                		pm.printDesc();
                	}else if(cmd[2].toLowerCase().equals("tardy")) {
                		pm.printTardy();
                	}
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println("Too few arguments.");
            } catch (NoSuchElementException e) {
                System.out.println("No such element.");
            }
        }
        ins.close();
    }
}
