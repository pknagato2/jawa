
public class Person extends base{
    private String lastName;
    private String name;
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getLastName() {
        return lastName;
    }
    
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    public Person(String name, String lastName) {
    	super();
    	this.name = name;
        this.lastName = lastName;
    }
    
    public Person(String name, String lastName, int id) {
        super(id);
    	this.name = name;
        this.lastName = lastName;
    }
    
    @Override
    public String getIdType() {
    	return "PersonId";
    }

    public void ShowDetails() {
        System.out.print(name + " " + lastName);
    }
    
    public int getId(){
    	return id;
    }
    
    public boolean equals(Person person) {
    	if(person.id == this.id)
    		return true;
    	return false;
    }
    
    public String ToString() {
    	return "Person: " + id + " " + name + " " + lastName;
    }
    
    public static Person getPersonFromString(String stringData) {
    	String [] arrOfStr = stringData.split(" ");
    	try {
    	if(arrOfStr.length != 4)
    		throw new wrongDataStringFormat("Zly format danych dla klasy person");
    	}catch(Exception e){
    		System.out.print(e.getMessage());
    	}
    	return new Person(arrOfStr[2], arrOfStr[3], Integer.parseInt(arrOfStr[1]));    	
    }
    
    
}
